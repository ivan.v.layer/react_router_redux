import React from 'react';
import './App.css';

import {
BrowserRouter,
Switch,
Routes,
Route,
Link,
NavLink,
} from 'react-router-dom';

import Home from './Pages/Home.js';
import Login from './Pages/Login.js';
import Register from './Pages/Register.js';
import About from './Pages/About.js';
import NotFound from './Pages/NotFound.js';
import ReduxExample from './Pages/ReduxExample.js';
import MobxExample from './MobX/mobx.js'

function App() {
  return (
  <div className="App">
    <p>
      Карта сайта:
    </p>
    <p><a href='/'>Логин</a></p>
    <p><a href='/Register'>Регистрация</a></p>
    <p><a href='/Home'>Главная страница</a></p>
    <p><a href='/About'>О сайте</a></p>
    <p><a href='/ReduxExample'>Redux Example</a></p>
    <p><a href='/MobxExample'>Mobx Example</a></p>

    <BrowserRouter>
      <Routes>
        <Route index element={<Login />} />
        <Route path="Home" element={<Home />} />
        <Route path="Register" element={<Register />} />
        <Route path="About" element={<About />} />
        <Route path="ReduxExample" element={<ReduxExample />} />
        <Route path="MobxExample" element={<MobxExample />} />
        <Route path = "*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>

  </div>
 );
}


export default App;
