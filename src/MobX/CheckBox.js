import React from "react";
import { observer } from "mobx-react";
import { Checkbox } from "antd";

import store from "./checkBoxStore";


function CheckBox() {
    return (
        <Checkbox checked={store.checked} onChange={() => store.checkedChange()} />
    );
}


export default observer(CheckBox);