import React from "react";
import { observer } from "mobx-react";

import store from "./checkBoxStore";

function Viewer() {
    if(store.checked)
    {
        return (<div><p>Вы видите это</p></div>);
    }
    else
    {
        return (<div></div>);
    }
}

export default observer(Viewer);