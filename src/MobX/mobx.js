import React from "react";

import CheckBox from "./CheckBox";
import Viewer from "./Viewer";

function Mobx() {
    return (
        <div  className= "MBOX">
            <CheckBox/>
            <Viewer/>
        </div>
    );

}

export default Mobx;