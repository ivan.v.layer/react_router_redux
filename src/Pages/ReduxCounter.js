import React from 'react'
import { connect } from 'react-redux'


// функция которая вызывается при изменении хранилища Redux
const mapStateToProps = (state) => {
  return {
    count: state.count
  }
}

// Диспетчер событий
const mapDispatchToProps = (dispatch_from_func_connect) => {
  return {
    onIncrement : () => {
      dispatch_from_func_connect( {type: 'INCREMENT'})
    },
    onDecrement: () => {
      dispatch_from_func_connect( {type: 'DECREMENT'} )
    }
  }
}

// Подключение хранилища Redux к компоненту через High Level Function
// Функция connect оборачивает наш компонент позволяя использовтать в нем состояния Redux
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(({ count, onDecrement, onIncrement }) => ( 
  // Сам компонент
  <div>
    <p>Count: {count}</p>
    <button onClick={onDecrement}>-</button>
    <button onClick={onIncrement}>+</button>
  </div>
))
