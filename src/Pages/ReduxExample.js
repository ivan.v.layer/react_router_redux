
import React from 'react'
import { render } from 'react-dom'

import { createStore } from 'redux'
import { Provider } from 'react-redux'

import reduxBusinessLogic from './reducers/reducerbusinesslogic'
import Counter from './ReduxCounter'

function ReduxExample() {
    return(
        <Provider store={createStore(reduxBusinessLogic)}>
            <Counter />
        </Provider>
        )
    }

export default ReduxExample;