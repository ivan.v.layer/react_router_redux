export default (state = { count: 0, text: "redux"}, action) => {
    switch (action.type) {
      case 'INCREMENT':
        return {
          count: state.count + 1
        }
      case 'DECREMENT':
        return {
          count: state.count - 1
        }
      case 'NECHTO':
        return {
          text: state.text = "redux"
        }
      default:
        return state
    }
  }